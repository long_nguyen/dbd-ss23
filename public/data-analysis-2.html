<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Collection and Analysis of Digital Behavioral Data</title>
    <meta charset="utf-8" />
    <meta name="author" content="Long Nguyen, Zaza Zindel" />
    <meta name="date" content="2023-06-29" />
    <script src="data-analysis-2_files/header-attrs/header-attrs.js"></script>
    <script src="data-analysis-2_files/xaringanExtra-progressBar/progress-bar.js"></script>
    <link href="data-analysis-2_files/panelset/panelset.css" rel="stylesheet" />
    <script src="data-analysis-2_files/panelset/panelset.js"></script>
    <link href="data-analysis-2_files/xaringanExtra-extra-styles/xaringanExtra-extra-styles.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/xaringan-unibi.css" type="text/css" />
    <link rel="stylesheet" href="assets/width.css" type="text/css" />
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@xz/fonts@1/serve/ibm-plex-mono.min.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: left, bottom, title-slide

.title[
# Collection and Analysis of<br>Digital Behavioral Data
]
.subtitle[
## Data Analysis II:<br>Word Embeddings and Their Applications
]
.author[
### Long Nguyen, Zaza Zindel
]
.date[
### 29 June 2023
]

---




<div>
<style type="text/css">.xaringan-extra-logo {
width: 200px;
height: 128px;
z-index: 0;
background-image: url(assets/faksoz.png);
background-size: contain;
background-repeat: no-repeat;
position: absolute;
top:1em;right:.5em;
}
</style>
<script>(function () {
  let tries = 0
  function addLogo () {
    if (typeof slideshow === 'undefined') {
      tries += 1
      if (tries < 10) {
        setTimeout(addLogo, 100)
      }
    } else {
      document.querySelectorAll('.remark-slide-content')
        .forEach(function (slide) {
          const logo = document.createElement('div')
          logo.classList = 'xaringan-extra-logo'
          logo.href = null
          slide.appendChild(logo)
        })
    }
  }
  document.addEventListener('DOMContentLoaded', addLogo)
})()</script>
</div><style>.xe__progress-bar__container {
  bottom:0;
  opacity: 1;
  position:absolute;
  right:0;
  left: 0;
}
.xe__progress-bar {
  height: 5px;
  background-color: #bfd02f;
  width: calc(var(--slide-current) / var(--slide-total) * 100%);
}
.remark-visible .xe__progress-bar {
  animation: xe__progress-bar__wipe 200ms forwards;
  animation-timing-function: cubic-bezier(.86,0,.07,1);
}
@keyframes xe__progress-bar__wipe {
  0% { width: calc(var(--slide-previous) / var(--slide-total) * 100%); }
  100% { width: calc(var(--slide-current) / var(--slide-total) * 100%); }
}</style>

&lt;style type="text/css"&gt;
pre {
  overflow: auto;
}
&lt;/style&gt;

### Outline

- Word embeddings&lt;br&gt;&lt;br&gt;
- Language modelling&lt;br&gt;&lt;br&gt;
- Transfer learning&lt;br&gt;&lt;br&gt;

.footnote[Link to slides: https://long_nguyen.pages.ub.uni-bielefeld.de/dbd-ss23/data-analysis-2]

---
class: inverse center middle

# Word embeddings

---

### Why do we use word embeddings?

- It is easy for humans to understand the relationship between words but for computers, this task may not be simple

- Many machine learning algorithms require numbers as input to perform any sort of job

--

&lt;br&gt;

- **Goal of word embedding algorithms**: Embed words with _meaning_ based on their similarity or relationship to other words.

---
background-color: white

### What is the meaning of a word?

Do you know what the word &lt;strong style="color: red;"&gt;tezgüino&lt;/strong&gt; means?

--

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/tezguino2-min.png" width="84%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### What is the meaning of a word?

How we come to understand the meaning of a word:

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/tezguino5-min.png" width="84%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### What is the meaning of a word?

How we come to understand the meaning of a word:

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/tezguino7-min.png" width="84%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### What is the meaning of a word?

How we come to understand the meaning of a word:

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/tezguino9-min.png" width="84%" style="display: block; margin: auto;" /&gt;

---

### What is a word embedding?

- The information about contexts of very word can be represented by a unique **vector of numbers**

--

- Word embeddings are high-dimensional to be able to capture enough information about how different words are

--

- For each word, the embedding captures the "meaning" of the word

--

- Similar words end up with similar embedding values

---
background-color: white

### Example of word embeddings

This is a word embedding for the word **king**


```
 [1]  0.504510  0.686070 -0.595170 -0.022801  0.600460 -0.134980 -0.088130
 [8]  0.473770 -0.617980 -0.310120 -0.076666  1.493000 -0.034189 -0.981730
[15]  0.682290  0.817220 -0.518740 -0.315030 -0.558090  0.664210  0.196100
[22] -0.134950 -0.114760 -0.303440  0.411770 -2.223000 -1.075600 -1.078300
[29] -0.343540  0.335050  1.992700 -0.042340 -0.643190  0.711250  0.491590
[36]  0.167540  0.343440 -0.256630 -0.852300  0.166100  0.401020  1.168500
[43] -1.013700 -0.215850 -0.151550  0.783210 -0.912410 -1.610600 -0.644260
[50] -0.510420
```

It is a list of 50 numbers. We can't tell much by looking at the values.

&lt;img src="https://jalammar.github.io/images/word2vec/king-colored-embedding.png" width="96%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### Example of word embeddings

Let's contrast **king** against other words:

&lt;img src="https://jalammar.github.io/images/word2vec/king-man-woman-embedding.png" width="80%" style="display: block; margin: auto;" /&gt;
--

- **Man** and **Woman** are much more similar to each other than either of them is to **king**

- These vector representations capture quite a bit of information and associations of these words

---
background-color: white

### Example of word embeddings

Here's an extended list of word embeddings:

&lt;img src="https://jalammar.github.io/images/word2vec/queen-woman-girl-embeddings.png" width="70%" style="display: block; margin: auto;" /&gt;

- What do you see in this example?

---
background-color: white

### Word embeddings allow for "calculating" similarities

And similarity is how meaning is captured.

--

We can perform simple algebra to arrive at analogies:

&lt;img src="https://jalammar.github.io/images/word2vec/king-analogy-viz.png" width="70%" style="display: block; margin: auto;" /&gt;

---
class: inverse center middle

# Language modelling

Or how word embeddings are created

---
background-color: white

### Training word vectors by predicting contexts

i.e. continuous skip-gram models, pioneered by Word2Vec ([Mikolov et al. 2013](https://arxiv.org/pdf/1301.3781.pdf))

--

- Take a huge text corpus

- Go over the text with a sliding window

- Compute probabilities of neighbour words (context) given the central words

- Adjust the vectors to increase these probabilities

--

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/w2v/window_prob1-min.png" width="80%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### Training word vectors by predicting contexts

i.e. continuous skip-gram models, pioneered by Word2Vec ([Mikolov et al. 2013](https://arxiv.org/pdf/1301.3781.pdf))

- Take a huge text corpus

- Go over the text with a sliding window

- Compute probabilities of neighbour words (context) given the central words

- Adjust the vectors to increase these probabilities

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/w2v/window_prob2-min.png" width="80%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### Training word vectors by predicting contexts

i.e. continuous skip-gram models, pioneered by Word2Vec ([Mikolov et al. 2013](https://arxiv.org/pdf/1301.3781.pdf))

- Take a huge text corpus

- Go over the text with a sliding window

- Compute probabilities of neighbour words (context) given the central words

- Adjust the vectors to increase these probabilities

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/w2v/window_prob3-min.png" width="80%" style="display: block; margin: auto;" /&gt;

---
background-color: white

### Training word vectors by predicting contexts

i.e. continuous skip-gram models, pioneered by Word2Vec ([Mikolov et al. 2013](https://arxiv.org/pdf/1301.3781.pdf))

- Take a huge text corpus

- Go over the text with a sliding window

- Compute probabilities of neighbour words (context) given the central words

- Adjust the vectors to increase these probabilities

&lt;img src="https://lena-voita.github.io/resources/lectures/word_emb/w2v/window_prob4-min.png" width="80%" style="display: block; margin: auto;" /&gt;

---

### GloVe: alternative to Word2Vec

- Global Vectors for Word Representation

- Generate word vectors by analysing global word-to-word co-occurrence counts

- Several pretrained GloVe vector representations are available in R via the [textdata](https://cran.r-project.org/package=textdata) package

--

&lt;br&gt;

- Word embeddings tutorial with code: &lt;https://long_nguyen.pages.ub.uni-bielefeld.de/dbd-ss23/word-embeddings.html&gt;

---
background-color: white

### Embedding size: dimensionality of embeddings

&lt;img src="https://jalammar.github.io/images/word2vec/word2vec-embedding-context-matrix.png" width="70%" style="display: block; margin: auto;" /&gt;

---

### Contextualised word embeddings

Representations of words in each particular context, pioneered by ELMo ([Peters et al. 2018](https://arxiv.org/pdf/1802.05365.pdf))

&lt;img src="https://jalammar.github.io/images/elmo-embedding-robin-williams.png" width="70%" style="display: block; margin: auto;" /&gt;

---
class: inverse center middle

# Transfer learning

Or using pretrained models for your tasks

---

### Transfer knowledge from one model to another

- You don't have a huge amount of data for the task you are interested in

- It is hard to get a good model using only this data

- You can get a lot of data for some other task like language modelling

--

&lt;br&gt;

→ You can "transfer" knowledge from the language models to your task. Examples:

- "Topic modelling" by performing [clustering on pretrained word embeddings](https://aclanthology.org/2020.emnlp-main.135.pdf)

- Zero-shot/few-shot text classification, e.g., for sentiment analysis

---

### Hugging Face 🤗

- Provides state-of-the-art pretrained models https://huggingface.co/models (BERT, GPT etc.)

- Available in R via the [text](https://www.r-text.org/) package:

--

&lt;br&gt;

- Text embeddings tutorial with code: &lt;https://long_nguyen.pages.ub.uni-bielefeld.de/dbd-ss23/text-embeddings.html&gt;

---

## Caution

Human prejudice or bias exists in the corpus used for training becomes imprinted into the vector data of the embeddings.

&gt; The models encode social biases, e.g. via stereotypes or negative sentiment towards certain groups. ([OpenAI](https://platform.openai.com/docs/guides/embeddings/limitations-risks))

---

### References and further readings

- Alammar, J (2019). The Illustrated Word2Vec [Blog post]. Retrieved from https://jalammar.github.io/illustrated-word2vec/

- Alammar, J (2018). The Illustrated BERT, ELMo, and co. (How NLP Cracked Transfer Learning) [Blog post]. Retrieved from https://jalammar.github.io/illustrated-bert/

- Voita, L (2020). NLP Course For You. Retrieved from https://lena-voita.github.io/nlp_course.html
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"ratio": "16:9",
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
