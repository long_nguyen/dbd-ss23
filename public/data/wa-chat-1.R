library(tidyverse)

chat_raw <- tibble(
  raw = read_lines("https://long_nguyen.pages.ub.uni-bielefeld.de/dbd-ss23/data/wa-chat-1.txt")
) |>
  slice(-1)

date_regex <- "^\\d{2}/\\d{2}/\\d{4}, \\d{2}:\\d{2}(?= - )"
person_regex <- "(?<=\\d - ).+?(?=:)"
emoji_regex <- "(?=[^0-9\\*\\#])\\p{Emoji}"

chat <- chat_raw |>
  mutate(
    person = str_extract(raw, person_regex),
    time = dmy_hm(str_extract(raw, date_regex)),
    message = str_replace(raw, paste0(date_regex, " - ", person_regex, ": "), ""),
    emojis = map_chr(str_extract_all(message, emoji_regex), paste0, collapse = "")
  ) |>
  fill(person, time) |>
  select(-raw)

chat
